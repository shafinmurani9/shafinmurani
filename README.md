# My introduction

- Hi, I’m Shafin Murani
- I’m interested in Ethical Hacking/Cyber Security
- I’m currently learning Python, C, JS(JavaScript) and PHP
- I’m looking to collaborate on any beginner level projects in the above listed languages
- Certifications : https://tinyurl.com/ewcv8z5h
- Age : 14

# Contact info
  - E-mail : shafinmurani9@gmail.com
  - Instagram : @shafin_murani
  - TryHackMe : www.tryhackme.com/p/shafin
  - Twitter : @ShafinMurani
  
